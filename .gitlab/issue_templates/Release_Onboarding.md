## [First Name], Welcome to GitLab!

We’re all excited you’ll be joining the Release team. This document is intended to be a reference point for you as you go through your onboarding and start to ramp up over the first couple months. Consider this document a reference. There are a lot of links here to various issues and pages in the Handbook that will be useful as you get comfortable with how things work at GitLab. This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager. 

---
### Your Onboarding Issue
This is where you’ll get started. It contains the better part of a week’s worth of onboarding tasks. There’s a lot to do in here, so be sure to pace yourself. It’s broken down by day to make it easier to digest.

[link to onboarding issue]

---
### Release Team

About the team:
[https://about.gitlab.com/handbook/engineering/ops/release/]()

Setup [Coffee Chats](https://about.gitlab.com/company/culture/all-remote/#coffee-chats) with everyone on the team

Product Roadmap:
[https://about.gitlab.com/direction/release/]()

Current Release [current release]
[link to current release]

---
### Release Onboarding Tasks

#### Before First Week

* [ ] Manager: Add to weekly team meetings, and monthly get together call
* [ ] Manager: Add to retro participants list
* [ ] Manager: Invite new team member to the following slack channels: #g_release, #backend or #frontend, #celebrations, #contribute2019, #development, #thanks
* [ ] Manager: Assign onboarding buddy

#### First Week
* [ ] Onboarding buddy: Schedule a 30 minute call with the new team member to introduce yourself. Make sure to walk through the new team member's calendar and ensure that the team meeting and other meetings are visible.
* [ ] Manager: Add to geekbot standup
* [ ] Manager: Schedule multiple checkins for the first 2 weeks

#### Second Week
* [ ] Onboarding buddy: Work closely with the new team member as they work on their development setup and on their getting started issues.
* [ ] New team member: Read the [Release Group](https://about.gitlab.com/handbook/engineering/ops/release/) page
* [ ] New team member: Schedule coffee chats with a few team members
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md), [Labels CE](https://gitlab.com/gitlab-org/gitlab-ce/labels)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.

---
### Developer Onboarding

Below are some general setup tasks. Some of these instructions may be duplicated in other places, but they are listed here for reference. These are specific to the areas of the GitLab that the Release group focuses on.

#### GDK
Get started by setting up the GDK ([GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)). Typically people do this two times, once for CE and once for EE.

#### GitLab Runner

1. [Setup Go environment and compile runner](https://docs.gitlab.com/runner/development/)
1. [Register runnerfor each GDK instance (CE and EE)](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md)

#### Sample Project
Create simple test-project in local gitlab and make tests pass. You can use [this guide](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-ruby-application-to-heroku.html). (remove staging and production sections of .gitlab-ci.yml if you don’t want to deploy the app)

#### GitLab Pages
The Release group owns GitLab Pages which is a separate Go application. 

You can find the code [here](https://gitlab.com/gitlab-org/gitlab-pages). Pull down the repo and get it running locally.

#### Auto DevOps

Setting up Auto DevOps locally can be time consuming, but it can be helpful to have it running. This doesn’t need to be done right away, so you can wait a few weeks on this.

[Auto DevOps Setup Instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md)

#### Frontend Engineers

* [ ] Familiarize yourself with the [Frontend development guidelines](https://docs.gitlab.com/ce/development/fe_guide/).
* [ ] Familiarize yourself with the [Vue documentation](https://vuejs.org/v2/guide/).
* [ ] Familiarize yourself with [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui).
* [ ] Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).

#### Additional Resources
Additional resources for setting up your local environment
[https://about.gitlab.com/handbook/developer-onboarding/](https://about.gitlab.com/handbook/developer-onboarding/)

---

### Process
#### Engineering Workflow

[https://about.gitlab.com/handbook/engineering/workflow/]()

#### Important Dates

[https://about.gitlab.com/handbook/product/#important-dates-pms-should-keep-in-mind]()

#### Workflow Labels

[https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels]()

#### Merge Request Workflow

[https://docs.gitlab.com/ee/development/code_review.html]()

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

[https://docs.gitlab.com/ee/development/rolling_out_changes_using_feature_flags.html]()
[https://docs.gitlab.com/ee/development/feature_flags.html]()

#### GitLab CE to GitLab EE merge

[https://docs.gitlab.com/ee/development/automatic_ce_ee_merge.html]()

#### Testing Best Practices

[https://docs.gitlab.com/ee/development/testing_guide/best_practices.html]()

---

#### Getting Started Tasks

Here are a couple tasks that I’ve found that you can take on when you’re ready to start getting in to code. These should be fairly straight forward, but if they turn out to be too complex let me know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar the process of shipping code at GitLab.

[add 2 to 3 getting started issues]

---

### Additional Notes

Feel free to edit this issue and add any notes you'd like to keep track of, or add comments below.